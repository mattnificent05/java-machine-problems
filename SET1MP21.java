import java.util.Scanner;
import java.text.DecimalFormat;
public class SET1MP21{
	public static void main (String[] args){
		
		double v,a;
		Scanner input = new Scanner(System.in);
		
		System.out.print("Enter v in meters/second(m/s):");
		v = input.nextDouble();
		System.out.print("Enter a in meters/second squared(m/s^2):");
		a = input.nextDouble();
		
		double length = (v * v) / (2 * a);

		DecimalFormat df = new DecimalFormat("###.###");
		System.out.printf("The minimum runway length for this airplane is " + df.format(length));
	}
}
		
		