import java.util.Scanner;

public class GratuityRate{
	public static void main(String[] args){
		
		Scanner input = new Scanner(System.in);
		
		System.out.print("Enter the subtotal and a gratuity rate: ");
		
		double gratuityRate, gratuity,total,subtotal;
		
		subtotal = input.nextDouble();
		gratuityRate = input.nextDouble();
		
		gratuity = subtotal * (gratuityRate / 100) ;
		total = subtotal + gratuity;
		
		System.out.print("The gratuity is " + gratuity + "and the total is " + total);
	}
}
		