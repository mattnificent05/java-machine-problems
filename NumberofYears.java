import java.util.Scanner;
public class NumberofYears{
	public static void main (String[] args){
		
		double minutes, years, days;
		
		Scanner input = new Scanner(System.in);
		
		System.out.print("Enter the Number of Minutes: ");
		minutes = input.nextDouble();
		
		years = minutes / 525600;
		days = (minutes % 525600) / 1440;
		
		System.out.println(minutes + " minutes and " + years + " years and " + days + " days ");
		
	}
	
}