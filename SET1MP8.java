import java.util.Scanner;
public class SET1MP8{
	public static void main (String[] args){
		
		int AsciiCode;
		Scanner input = new Scanner(System.in);
		
		System.out.print("Enter an ASCII code:");
		AsciiCode = input.nextInt();
		
		char displayCharacter = (char) AsciiCode;
		
		System.out.print("The Character for ASCII code " + AsciiCode + " is " + displayCharacter);
		
	}
}
		