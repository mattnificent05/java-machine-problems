import java.util.Scanner;
public class AreaofHexagon{
	public static void main (String[] args){
		
	Scanner input = new Scanner(System.in);
	
	Double side;
	Double area;
		
	System.out.print("Enter the side: ");
	side = input.nextDouble();
		
	area = (6 * side * side ) / ( 4 * Math.tan(Math.PI/6));
		
	
	System.out.printf("The area of the hexagon is %.2f", area);
	}

}