import java.util.Scanner;
import java.text.DecimalFormat;
public class SET1MP4{
	public static void main (String[] args){
		
		double FirstInput, m;
		
		Scanner input = new Scanner(System.in);
		
		System.out.print("Enter a number in pounds: ");
		FirstInput = input.nextDouble();
		
		m = FirstInput * 0.45359237;
		DecimalFormat df = new DecimalFormat("##.###");
		System.out.print(FirstInput + " pounds is " + df.format(m) + " kilograms");
	}
}