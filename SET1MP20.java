import java.util.Scanner;
import java.lang.Math;
public class SET1MP20{
	public static void main (String[] args){
	
	double startingVelocity, endingVelocity, time, average;
	Scanner input = new Scanner(System.in);
	
	System.out.print("Enter starting velocity(v0) in meter/second: ");
        startingVelocity = input.nextDouble();
         
        System.out.print("Enter ending velocity(v1) in meter/second: ");
        endingVelocity = input.nextDouble();
         
        System.out.print("Enter time(t) in seconds: ");
        time = input.nextDouble();
         
        average = (endingVelocity - startingVelocity) / time;
         
        System.out.print("The average acceleration is " + Math.round(average * 10000) / 10000.0);
    }
}
	