import java.util.Scanner;
import java.text.DecimalFormat;
import java.lang.Math;
public class SET1MP12{
	public static void main (String[] args){
	
	double investmentAmount, monthlyInterestRate, numberOfYears, futureInvestmentValue;
	
	Scanner input = new Scanner(System.in);
	
	System.out.print("Enter investment amount: ");
	investmentAmount = input.nextDouble();
	
	System.out.print("Enter monthly interest rate: ");
	monthlyInterestRate = input.nextDouble();
	
	System.out.print("Enter number of years: ");
	numberOfYears = input.nextDouble();
	
	futureInvestmentValue = investmentAmount * Math.pow((1 + (monthlyInterestRate / 1200)), (numberOfYears * 12));
	futureInvestmentValue = Math.round(futureInvestmentValue * 100) / 100.0;
	DecimalFormat df = new DecimalFormat("####.##");
	System.out.printf("Accumulated value is " + futureInvestmentValue);
	
	}
}
	