import java.util.Scanner;

public class computingBMI{
	public static void main (String[] args){
	
	Scanner input = new Scanner(System.in);
	
	System.out.print("Enter weight in pounds:");
	double weight = input.nextDouble();
	System.out.print("Enter height in inches:");
	double height = input.nextDouble();
	
	weight = weight * 0.45359237;
	height = height * 0.0254;
	
	double bmI = weight / (height * height);
	bmI = (int) (bmI * 10000) / 10000.0;
	
	System.out.print("BMI is " + bmI);
	}
}