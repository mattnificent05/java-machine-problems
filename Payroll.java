import java.util.Scanner;

public class Payroll{
	public static void main (String[] args){
	
	Scanner input = new Scanner(System.in);
	
	System.out.print("Enter employee's name: ");
	String name = input.next();
	System.out.print("Enter number of hours worked in a week: ");
	double hoursworked = input.nextDouble();
	System.out.print("Enter hourly pay rate: ");
	double hourlyPayRate = input.nextDouble();
	System.out.print("Enter federal tax withholding rate: ");
	Double federalTaxRate = input.nextDouble();
	System.out.print("Enter state tax withholding rate: ");
	Double stateTaxRate = input.nextDouble();
	
	double grossPay = hoursworked * hourlyPayRate;
	double fedTaxRate =  grossPay * federalTaxRate;
	double stateTax = grossPay * stateTaxRate;
	double totalDeduction = fedTaxRate + stateTax;
	double netPay = grossPay - totalDeduction;
	
	netPay = (int)(netPay * 100)/100.0;
	
	System.out.println("Employee name: " + name);
	System.out.println("Hours worked: " + hoursworked);
	System.out.println("Pay Rate: " + hourlyPayRate);
	System.out.println("Gross pay: " + grossPay);
	System.out.println("Deduction: " + name);
	System.out.println("\tFederal Withholding (" + (double)federalTaxRate + "%): " + fedTaxRate);
	System.out.println("\tState Withholding (" + (double)stateTaxRate + "%): " + stateTax);
	System.out.println("\tTotal Deduction: " + totalDeduction);
	System.out.println("Net Pay: " + netPay);
	
	
	}
}
	
	
	