import java.util.Scanner;
import java.text.DecimalFormat;
public class SET1MP16{
	public static void main (String[] args){
	
	double temperature, milesPerHour, windChillIndex, speed;
	
	Scanner input = new Scanner(System.in);
	DecimalFormat df = new DecimalFormat("#.#####");
	
	System.out.print("Enter the Temperature in Farenheit: ");
	temperature = input.nextDouble();
	
	System.out.print("Enter the wind speed miles per hour: ");
	milesPerHour = input.nextDouble();
	
	windChillIndex = 35.74 + 0.6215 * temperature - 35.75 * Math.pow(milesPerHour, 0.16) + 0.4275 * temperature * Math.pow(milesPerHour, 0.16);
	System.out.print("The wind chill index is " + df.format(windChillIndex));
	}
}
