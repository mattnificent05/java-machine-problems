import java.util.Scanner;
public class SET1MP6{
	public static void main(String[] args){
		
		int value, firstValue,secondValue,thirdValue, answer;
		Scanner input = new Scanner(System.in);
	
		System.out.println("Enter a number between 0 and 1000: ");
		value = input.nextInt();
		
		firstValue = value % 10;
		secondValue = (value %100)/10;
		thirdValue = value / 100;
		
		answer = firstValue + secondValue + thirdValue;
		
		System.out.println("The sum of the digits is >> " + answer);
		
	}
}